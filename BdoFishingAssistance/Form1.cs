﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
namespace BdoAssistance
{
    
    public partial class Form1 : Form
    {
        private static readonly IntPtr HWND_TOPMOST = new IntPtr(-1);
        private const UInt32 SWP_NOSIZE = 0x0001;
        private const UInt32 SWP_NOMOVE = 0x0002;
        private const UInt32 TOPMOST_FLAGS = 0x0040;
        private RichTextBox logTxt;
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, SetWindowPosFlags uFlags);
        public Form1()
        {
            InitializeComponent();
        }

        [Flags]
        public enum SetWindowPosFlags : uint
        {
            SWP_ASYNCWINDOWPOS = 0x4000,

            SWP_DEFERERASE = 0x2000,

            SWP_DRAWFRAME = 0x0020,

            SWP_FRAMECHANGED = 0x0020,

            SWP_HIDEWINDOW = 0x0080,

            SWP_NOACTIVATE = 0x0010,

            SWP_NOCOPYBITS = 0x0100,

            SWP_NOMOVE = 0x0002,

            SWP_NOOWNERZORDER = 0x0200,

            SWP_NOREDRAW = 0x0008,

            SWP_NOREPOSITION = 0x0200,

            SWP_NOSENDCHANGING = 0x0400,

            SWP_NOSIZE = 0x0001,

            SWP_NOZORDER = 0x0004,

            SWP_SHOWWINDOW = 0x0040,
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            Guid g = Guid.NewGuid();
            string GuidString = Convert.ToBase64String(g.ToByteArray());
            GuidString = GuidString.Replace("=", "");
            GuidString = GuidString.Replace("+", "");
            this.Text = GuidString;
            int y = Screen.PrimaryScreen.Bounds.Bottom - logTxt.Height;
            Console.WriteLine(y);
            SetWindowPos(Handle, HWND_TOPMOST, 0, y, 285, 263, SetWindowPosFlags.SWP_SHOWWINDOW);
        }

        public void LogTextEvent(string EventText)
        {
            if (logTxt.InvokeRequired)
            {
                logTxt.BeginInvoke(new Action(delegate {
                    LogTextEvent(EventText);
                }));
                return;
            }

            string nDateTime = DateTime.Now.ToString("hh:mm:ss tt") + " - ";


            // newline if first line, append if else.
            if (logTxt.Lines.Length == 0)
            {
                logTxt.AppendText(nDateTime + EventText);
                logTxt.ScrollToCaret();
                logTxt.AppendText(System.Environment.NewLine);
            }
            else
            {
                logTxt.AppendText(nDateTime + EventText + System.Environment.NewLine);
                logTxt.ScrollToCaret();
            }
        }

        private void logTxt_TextChanged(object sender, EventArgs e)
        {
            logTxt.Refresh();
        }

        private void InitializeComponent()
        {
            this.logTxt = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // logTxt
            // 
            this.logTxt.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.logTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.logTxt.ForeColor = System.Drawing.Color.Red;
            this.logTxt.Location = new System.Drawing.Point(0, 0);
            this.logTxt.Name = "logTxt";
            this.logTxt.ReadOnly = true;
            this.logTxt.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.logTxt.Size = new System.Drawing.Size(285, 263);
            this.logTxt.TabIndex = 0;
            this.logTxt.Text = "";
            this.logTxt.TextChanged += new System.EventHandler(this.logTxt_TextChanged);
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.logTxt);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_Closed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        private void Form1_Closed(object sender, FormClosedEventArgs e)
        {
            Program.OnApplicationExit(sender,e);
        }
    }
}
