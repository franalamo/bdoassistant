﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Structure;
using Tesseract;
using Capture;
using Capture.Interface;
using Capture.Hook;
using InputManager;
using AForge.Imaging.Filters;
using AForge;
using System.Runtime.InteropServices;
using System.IO;
using BdoFishingAssistance;
using System.Drawing.Drawing2D;
using System.Collections.Generic;
using OpenCvSharp;
using System.Linq;
using Emgu.CV.Util;
using Emgu.CV.CvEnum;

namespace BdoAssistance
{

    internal class Program
    {


        public static System.Drawing.Point Compare(Image<Bgr, byte> scr1, Image<Bgr, byte> scr2, double precision)
        {
            using (Image<Gray, float> result = scr1.MatchTemplate(scr2, Emgu.CV.CvEnum.TemplateMatchingType.CcoeffNormed))
            {
                double[] minValues, maxValues;
                System.Drawing.Point[] minLocations, maxLocations;
                result.MinMax(out minValues, out maxValues, out minLocations, out maxLocations);

                // You can try different values of the threshold. I guess somewhere between 0.75 and 0.95 would be good.
                if (maxValues[0] > precision)
                {
                    return new System.Drawing.Point(maxLocations[0].X + (scr2.Size.Width / 3), maxLocations[0].Y + (scr2.Size.Height / 3));

                }
            }
            return new System.Drawing.Point(0, 0);
        }

        public static Image RotateImage(Image img, float rotationAngle)
        {
            //create an empty Bitmap image
            Bitmap bmp = new Bitmap(img.Width, img.Height);

            //turn the Bitmap into a Graphics object
            Graphics gfx = Graphics.FromImage(bmp);

            //now we set the rotation point to the center of our image
            gfx.TranslateTransform((float)bmp.Width / 2, (float)bmp.Height / 2);

            //now rotate the image
            gfx.RotateTransform(rotationAngle);

            gfx.TranslateTransform(-(float)bmp.Width / 2, -(float)bmp.Height / 2);

            //set the InterpolationMode to HighQualityBicubic so to ensure a high
            //quality image once it is transformed to the specified size
            gfx.InterpolationMode = InterpolationMode.HighQualityBicubic;

            //now draw our new image onto the graphics object
            gfx.DrawImage(img, new System.Drawing.Point(0, 0));

            //dispose of our Graphics object
            gfx.Dispose();

            //return the image
            return bmp;
        }
        static Dictionary<int, String> map = new Dictionary<int, String>();
        static List<String> texts = new List<string>();
        public static void CompareArrows(Bitmap scr1param, double precision)
        {

            Image<Bgr, byte> scr1 = new Image<Bgr, byte>(scr1param);
            Image<Bgr, byte> W = new Image<Bgr, byte>("W.bmp");
            Image<Bgr, byte> A = new Image<Bgr, byte>("A.bmp");
            Image<Bgr, byte> S = new Image<Bgr, byte>("S.bmp");
            Image<Bgr, byte> D = new Image<Bgr, byte>("D.bmp");

            for(int index = 0; index < 4; index++)
            {
                int ang = index * 90;
                int j = 0;
                Image<Bgr, byte> scr2 = null;
                switch (ang)
                {
                    case 0:
                        scr2 = W;
                        break;
                    case 90:
                        scr2 = A;
                        break;
                    case 180:
                        scr2 = S;
                        break;
                    case 270:
                        scr2 = D;
                        break;
                }
                while (true)
                {
                    using (Image<Gray, float> result = scr1.MatchTemplate(scr2, Emgu.CV.CvEnum.TemplateMatchingType.CcoeffNormed))
                    {
                        var ran = new Random().Next();
                        double[] minValues, maxValues;
                        System.Drawing.Point[] minLocations, maxLocations;
                        result.MinMax(out minValues, out maxValues, out minLocations, out maxLocations);
                        Console.WriteLine("PRECISION: " + maxValues[0]);
                        if (maxValues[0] > precision)
                        {
                            if (ang == 0) //UP
                            {
                                try
                                {
                                    if (numberBet(maxLocations[0].X, "w"))
                                    {
                                            map.Add(maxLocations[0].X, "w");  
                                    }
                                }
                                catch
                                {
                                    Console.WriteLine(maxLocations[0].X + ": w ya en lista");
                                }
                                Rectangle match = new Rectangle(maxLocations[0], scr2.Size);
                                scr1.Draw(match, new Bgr(Color.Blue), -1);
                                scr1.Save("C:\\tmp\\ARROWS" + index + "_" + j + "_"+ ran+ ".bmp");

                            }
                            if (ang == 90) //LEFT
                            {
                                try
                                {
                                    if (numberBet(maxLocations[0].X, "a"))
                                    {
                                            map.Add(maxLocations[0].X, "a");
                                    }
                                }
                                catch
                                {
                                    Console.WriteLine(maxLocations[0].X + ": a ya en lista");
                                }
                                Rectangle match = new Rectangle(maxLocations[0], scr2.Size);
                                scr1.Draw(match, new Bgr(Color.Blue), -1);
                                scr1.Save("C:\\tmp\\ARROWS" + index + "_" + j + "_" + ran + ".bmp");
                            }
                            if (ang == 180) //down
                            {
                                try
                                {
                                    if (numberBet(maxLocations[0].X, "s"))
                                    {
                                         map.Add(maxLocations[0].X, "s");
                                    }
                                }
                                catch
                                {
                                    Console.WriteLine(maxLocations[0].X + ": s ya en lista");
                                }
                                Rectangle match = new Rectangle(maxLocations[0], scr2.Size);
                                scr1.Draw(match, new Bgr(Color.Blue), -1);
                                scr1.Save("C:\\tmp\\ARROWS" + index + "_" + j + "_" + ran + ".bmp");
                            }
                            if (ang == 270) //right
                            {
                                try
                                {
                                    if (numberBet(maxLocations[0].X, "d"))
                                    {
                                         map.Add(maxLocations[0].X, "d");
                                    }
                                }
                                catch
                                {
                                    Console.WriteLine(maxLocations[0].X + ": d ya en lista");
                                }
                                Rectangle match = new Rectangle(maxLocations[0], scr2.Size);
                                scr1.Draw(match, new Bgr(Color.Blue), -1);
                                scr1.Save("C:\\tmp\\ARROWS" + index + "_" + j + "_" + ran + ".bmp");
                            }
                            j++;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
        }

        public static List<String> getStringMap()
        {

            // Acquire keys and sort them.
            var items = from pair in map
                        orderby pair.Key ascending
                        select pair;
            // Display results.

            foreach (KeyValuePair<int, String> pair in items)
            {
                Console.WriteLine("{0}: {1}", pair.Key, pair.Value);
                texts.Add(pair.Value);
            }
            return texts;
        }

        public static bool numberBet(int key, String value)
        {
            lock (value)
            {
                var items = from pair in map
                            orderby pair.Key ascending
                            select pair;
                // Display results.

                foreach (KeyValuePair<int, String> pair in items)
                {
                    if (pair.Value == value)
                    {
                        if (Math.Abs(pair.Key - key) < 40)
                        {
                            Console.WriteLine("ENCONTRADO : " + key + ":" + value + " | " + pair.Key + ":" + pair.Value);
                            return false;
                        }
                    }

                }
                return true;
            }   
        }

        public static void SetAndGoLeft(System.Drawing.Point point)
        {
            Mouse.Move(point.X, point.Y);
            Thread.Sleep(500);
            Mouse.SendButton(Mouse.MouseButtons.LeftDown);
            Thread.Sleep(10);
            Mouse.SendButton(Mouse.MouseButtons.LeftUp);
        }

        public static void SetAndGoRight(System.Drawing.Point point)
        {
            Mouse.Move(point.X, point.Y);
            Thread.Sleep(500);
            Mouse.SendButton(Mouse.MouseButtons.RightDown);
            Thread.Sleep(10);
            Mouse.SendButton(Mouse.MouseButtons.RightUp);
        }

        public static double GetRandomNumber(double minimum, double maximum)
        {
            Random random = new Random();
            return random.NextDouble() * (maximum - minimum) + minimum;
        }

        public static String SearchKeysOSD(Bitmap bitmap)
        {
            try
            {
                using (var engine = new TesseractEngine(@"./tessdata", "osd", EngineMode.Default))
                {
                    using (var page = engine.Process(bitmap))
                    {
                        var text = page.GetText();
                        if (page.GetMeanConfidence() > 0.5)
                        {
                            return text;
                        }
                        return "";
                    }
                }
            }
            catch (Exception e)
            {
                Trace.TraceError(e.ToString());
                Console.WriteLine("Unexpected Error: " + e.Message);
                Console.WriteLine("Details: ");
                Console.WriteLine(e.ToString());
                return null;
            }
        }

        public static String SearchKeys(Bitmap bitmap, double precission)
        {
            try
            {
                using (var engine = new TesseractEngine(@"./tessdata", "eng", EngineMode.Default))
                {
                    using (var page = engine.Process(bitmap))
                    {
                        var text = page.GetText();
                        if (page.GetMeanConfidence() > precission)
                        {
                            return text;
                        }
                        return "";
                    }
                }
            }
            catch (Exception e)
            {
                Trace.TraceError(e.ToString());
                Console.WriteLine("Unexpected Error: " + e.Message);
                Console.WriteLine("Details: ");
                Console.WriteLine(e.ToString());
                return null;
            }
        }
        public static void logInit(Form1 form)
        {
            form.ShowDialog();
        }

        private void btnInject_Click(object sender, EventArgs e)
        {
            if (_captureProcess == null)
            {
                AttachProcess();
            }
            else
            {
                HookManager.RemoveHookedProcess(_captureProcess.Process.Id);
                _captureProcess.CaptureInterface.Disconnect();
                _captureProcess = null;
            }
        }

        static int processId = 0;
        static Process _process;
        static CaptureProcess _captureProcess;
        private static void AttachProcess()
        {
            Process process = Process.GetProcessesByName("BlackDesert64")[0];
            // Simply attach to the first one found.

            // If the process doesn't have a mainwindowhandle yet, skip it (we need to be able to get the hwnd to set foreground etc)
            if (process.MainWindowHandle == IntPtr.Zero)
            {
                return;
            }

            // Skip if the process is already hooked (and we want to hook multiple applications)
            if (HookManager.IsHooked(process.Id))
            {
                return;
            }

            Direct3DVersion direct3DVersion = Direct3DVersion.Direct3D11;

            CaptureConfig cc = new CaptureConfig()
            {
                Direct3DVersion = direct3DVersion,
                ShowOverlay = true
            };

            processId = process.Id;
            _process = process;

            var captureInterface = new CaptureInterface();
            _captureProcess = new CaptureProcess(process, cc, captureInterface);

            Thread.Sleep(10);

            if (_captureProcess == null)
            {
                MessageBox.Show("No executable found matching: BlackDesert64");
            }
        }

        private static Form1 form;
        private static Bitmap bitmap;
        private static Rectangle rect;
        static DateTime start;
        static DateTime end;

        private static void CapturarPrograma()
        {
            start = DateTime.Now;
            DoRequest();
        }

        /// <summary>
        /// Create the screen shot request
        /// </summary>
        static void DoRequest()
        {

            _captureProcess.BringProcessWindowToFront();
            // Initiate the screenshot of the CaptureInterface, the appropriate event handler within the target process will take care of the rest
            _captureProcess.CaptureInterface.BeginGetScreenshot(new Rectangle(0, 0, 0, 0), new TimeSpan(0, 0, 0, 0, 200), Callback, null, Capture.Interface.ImageFormat.Bitmap);
        }
        //Resize
        public static Bitmap Resize(Bitmap bmp, int newWidth, int newHeight, bool removeNoise, bool setGrayScale, bool GSbpp)
        {

            Bitmap temp = (Bitmap)bmp;
            Bitmap bmap = new Bitmap(newWidth, newHeight, PixelFormat.Format24bppRgb);



            double nWidthFactor = (double)temp.Width / (double)newWidth;
            double nHeightFactor = (double)temp.Height / (double)newHeight;

            double fx, fy, nx, ny;
            int cx, cy, fr_x, fr_y;
            Color color1 = new Color();
            Color color2 = new Color();
            Color color3 = new Color();
            Color color4 = new Color();
            byte nRed, nGreen, nBlue;

            byte bp1, bp2;

            for (int x = 0; x < bmap.Width; ++x)
            {
                for (int y = 0; y < bmap.Height; ++y)
                {

                    fr_x = (int)Math.Floor(x * nWidthFactor);
                    fr_y = (int)Math.Floor(y * nHeightFactor);
                    cx = fr_x + 1;
                    if (cx >= temp.Width) cx = fr_x;
                    cy = fr_y + 1;
                    if (cy >= temp.Height) cy = fr_y;
                    fx = x * nWidthFactor - fr_x;
                    fy = y * nHeightFactor - fr_y;
                    nx = 1.0 - fx;
                    ny = 1.0 - fy;

                    color1 = temp.GetPixel(fr_x, fr_y);
                    color2 = temp.GetPixel(cx, fr_y);
                    color3 = temp.GetPixel(fr_x, cy);
                    color4 = temp.GetPixel(cx, cy);

                    // Blue
                    bp1 = (byte)(nx * color1.B + fx * color2.B);

                    bp2 = (byte)(nx * color3.B + fx * color4.B);

                    nBlue = (byte)(ny * (double)(bp1) + fy * (double)(bp2));

                    // Green
                    bp1 = (byte)(nx * color1.G + fx * color2.G);

                    bp2 = (byte)(nx * color3.G + fx * color4.G);

                    nGreen = (byte)(ny * (double)(bp1) + fy * (double)(bp2));

                    // Red
                    bp1 = (byte)(nx * color1.R + fx * color2.R);

                    bp2 = (byte)(nx * color3.R + fx * color4.R);

                    nRed = (byte)(ny * (double)(bp1) + fy * (double)(bp2));

                    bmap.SetPixel(x, y, System.Drawing.Color.FromArgb
            (255, nRed, nGreen, nBlue));
                }
            }
            if (setGrayScale)
            {
                if (GSbpp)
                {
                    Grayscale filter = new Grayscale(0.5, 0.5, 0.5);
                    // apply the filter
                    bmap = filter.Apply(bmap);
                }
                else
                {
                    bmap = SetGrayscale(bmap);
                }
            }
            if (removeNoise)
            {
                bmap = RemoveNoise(bmap); 
            }
            return bmap;

        }


        //SetGrayscale
        public static Bitmap SetGrayscale(Bitmap img)
        {

            Bitmap temp = (Bitmap)img;
            Bitmap bmap = (Bitmap)temp.Clone();
            
            for (int i = 0; i < bmap.Width; i++)
            {
                Parallel.For(0, bmap.Height, j => //PARALLEL EXAMPLE (IT CAN BE USE IN 'NORMAL' FOR)
                {
                    Color c = bmap.GetPixel(i, j);
                    byte gray = (byte)(.299 * c.R + .587 * c.G + .114 * c.B);
                    lock(bmap){ bmap.SetPixel(i, j, Color.FromArgb(gray, gray, gray)); } //CRITIC BMAP
                }
                );  
            }
            return (Bitmap)bmap.Clone();

        }
        //RemoveNoise
        public static Bitmap RemoveNoise(Bitmap bmap)
        {

            for (var x = 0; x < bmap.Width; x++)
            {
                for (var y = 0; y < bmap.Height; y++)
                {
                    var pixel = bmap.GetPixel(x, y);
                    if (pixel.R < 162 && pixel.G < 162 && pixel.B < 162)
                        bmap.SetPixel(x, y, Color.Black);
                    else if (pixel.R > 162 && pixel.G > 162 && pixel.B > 162)
                        bmap.SetPixel(x, y, Color.White);
                }
            }

            return bmap;
        }


        private static Bitmap cloneAforge(Bitmap bmp, int startX, int startY, int width, int height)
        {
            BitmapData rawOriginal = bmp.LockBits(new Rectangle(startX, startY, width, height), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            Bitmap cloneBitmap = AForge.Imaging.Image.Clone(rawOriginal);
            bmp.UnlockBits(rawOriginal);
            return cloneBitmap;
        }

        private static Bitmap getRedBitmap(Bitmap bitmap)
        {
            Bitmap temp = new Bitmap(bitmap);
            Bitmap bmap = (Bitmap)temp.Clone();
            Color c;
            for (int i = 0; i < bmap.Width; i++)
            {
                for (int j = 0; j < bmap.Height; j++)
                {
                    c = bmap.GetPixel(i, j);
                    byte gray = (byte)(.9 * c.R + .1 * c.G + .1 * c.B);

                    bmap.SetPixel(i, j, Color.FromArgb(gray, gray, gray));
                }
            }
            return (Bitmap)bmap.Clone();
        }

        private static Bitmap getBlueBitmap(Bitmap bitmap)
        {
            Bitmap temp = (Bitmap)bitmap;
            Bitmap bmap = (Bitmap)temp.Clone();
            Color c;
            for (int i = 0; i < bmap.Width; i++)
            {
                for (int j = 0; j < bmap.Height; j++)
                {
                    c = bmap.GetPixel(i, j);
                    byte gray = (byte)(.1 * c.R + .1 * c.G + .9 * c.B);

                    bmap.SetPixel(i, j, Color.FromArgb(gray, gray, gray));
                }
            }
            return (Bitmap)bmap.Clone();
        }

        private static Bitmap getGreenBitmap(Bitmap bitmap)
        {
            Bitmap temp = (Bitmap)bitmap;
            Bitmap bmap = (Bitmap)temp.Clone();
            Color c;
            for (int i = 0; i < bmap.Width; i++)
            {
                for (int j = 0; j < bmap.Height; j++)
                {
                    c = bmap.GetPixel(i, j);
                    byte gray = (byte)(.1 * c.R + .9 * c.G + .1 * c.B);

                    bmap.SetPixel(i, j, Color.FromArgb(gray, gray, gray));
                }
            }
            return (Bitmap)bmap.Clone();
        }

        private static string reconhecerCaptcha(Image img, int i)
        {
            Bitmap imagem = new Bitmap(img);
            imagem = imagem.Clone(new Rectangle(0, 0, img.Width, img.Height), System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            Erosion erosion = new Erosion();
            Dilatation dilatation = new Dilatation();
            Invert inverter = new Invert();
            ColorFiltering cor = new ColorFiltering();
            cor.Blue = new AForge.IntRange(200, 255);
            cor.Red = new AForge.IntRange(200, 255);
            cor.Green = new AForge.IntRange(200, 255);
            Opening open = new Opening();
            BlobsFiltering bc = new BlobsFiltering();
            Closing close = new Closing();
            GaussianSharpen gs = new GaussianSharpen();
            ContrastCorrection cc = new ContrastCorrection();
            bc.MinHeight = 10;
            FiltersSequence seq = new FiltersSequence(gs, inverter, open, inverter, bc, inverter, open, cc, cor, bc, inverter);
            Bitmap imgFilter = seq.Apply(imagem);
            imgFilter.Save("C:\\tmp\\src"+i+".bmp");
            string reconhecido = OCR(imgFilter);
            return reconhecido;
        }

        private static string OCR(Bitmap b)
        {
            string res = "";
            using (var engine = new TesseractEngine(@"tessdata", "eng", EngineMode.Default))
            {
                engine.SetVariable("tessedit_char_whitelist", "AWSD");
                engine.SetVariable("tessedit_unrej_any_wd", true);

                using (var page = engine.Process(b, PageSegMode.SingleLine))
                {
                    res = page.GetText();
                    Console.WriteLine(page.GetMeanConfidence());
                    if (page.GetMeanConfidence() > 0.25)
                    {
                        return res;
                    }
                }
                    
            }
            return "";
        }

        private static Bitmap brighnessRemove(Bitmap bitmap)
        {
            const float minlimit = 100;
            const float maxlimit = 239;
            const float minlimitBrightness = 0.4f;
            const float maxlimitBrightness = 0.6f;
            for (int i = 0; i < bitmap.Width; i++)
            {
                for (int j = 0; j < bitmap.Height; j++)
                {
                    Color c = bitmap.GetPixel(i, j);
                    //Console.WriteLine(c.GetBrightness());//(c.R < minlimit && c.G < minlimit && c.B < minlimit) || (c.R > maxlimit && c.G > maxlimit && c.B > maxlimit) ||
                    if ((c.GetBrightness() < minlimitBrightness || c.GetBrightness() > maxlimitBrightness))
                    {
                        bitmap.SetPixel(i, j, Color.White);
                    }
                    /*
                    if (c.R > minlimit && c.G > minlimit && c.B > minlimit && c.R < maxlimit && c.G < maxlimit && c.B < maxlimit)
                    {
                        bitmap.SetPixel(i, j, Color.Black);
                    }*/
                }
            }
            return bitmap;
        }

        public static byte[] ImageToByte(Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }
        static int iteration = 0;

        static int aciertos = 0;
        static int fallos = 0;
        /// <summary>
        /// The callback for when the screenshot has been taken
        /// </summary>
        /// <param name="clientPID"></param>
        /// <param name="status"></param>
        /// <param name="screenshotResponse"></param>
        static void Callback(IAsyncResult result)
        {
            using (Screenshot screenshot = _captureProcess.CaptureInterface.EndGetScreenshot(result))

                try
                {
                    _captureProcess.BringProcessWindowToFront();
                    switch (codigo)
                    {
                        case 0:
                            //Buscar Caña
                            if (screenshot != null && screenshot.Data != null)
                            {
                                bitmap = screenshot.ToBitmap();
                                bitmap = cloneAforge(bitmap, rect.X, rect.Y, rect.Width, rect.Height);
                                return;
                            }
                            break;
                        case 1:
                            //Buscar pesca
                            if (screenshot != null && screenshot.Data != null)
                            {
                                bitmap = screenshot.ToBitmap();
                                bitmap = cloneAforge(bitmap, rect.X, rect.Y, rect.Width, rect.Height);
                                bitmap = Resize(bitmap, bitmap.Width, bitmap.Height, true,true,false);
                                text = SearchKeys(bitmap, 0.5);
                                if (text.Contains("bite"))
                                {
                                    encontrado = true;
                                    return;
                                }
                            }
                            break;
                        case 2:
                            //Buscar Minijuego Barra
                            if (screenshot != null && screenshot.Data != null)
                            {
                                bitmap = screenshot.ToBitmap();
                                bitmap = cloneAforge(bitmap, rect.X, rect.Y, rect.Width, rect.Height);
                                Benchmark.Start();
                                int blueAverage = colorEverage(bitmap);
                                Benchmark.End();
                                double t = Benchmark.GetSeconds();
                                Console.WriteLine("TIME BAR: " + t);
                                if (blueAverage > 140)
                                {
                                    Keyboard.KeyPress(Keys.Space);
                                    encontrado = true;
                                    Console.WriteLine("COLOR ACEPTADO: BlueEverage: " + blueAverage);
                                    bitmap.Save("C:\\tmp\\BAR.bmp");
                                    return;
                                }
                            }
                            break;
                        case 3:
                            //Buscar Minijuego Flechas
                            if (screenshot != null && screenshot.Data != null)
                            {

                                Random r = new Random();
                                int a = 0;
                                
                                bitmap = screenshot.ToBitmap();
                                bitmap = cloneAforge(bitmap, rect.X, rect.Y, rect.Width, rect.Height);
                                Benchmark.Start();
                                a = r.Next();
                                Bitmap bmp1 = (Bitmap)bitmap.Clone();
                                ResizeBilinear filterS = new ResizeBilinear(bmp1.Width * 4, bmp1.Height * 4);
                                // apply the filter
                                bmp1 = filterS.Apply(bmp1);
                                Grayscale filterGS = new Grayscale(0, 0, 1);
                                // apply the filter
                                bmp1 = filterGS.Apply(bmp1);
                                Threshold filter = new Threshold(160);
                                // apply the filter
                                filter.ApplyInPlace(bmp1);
                                bmp1.Save("C:\\tmp\\imagen1_threshold" + a + ".bmp");
                                String textOCR = OCR(bmp1);
                                Benchmark.End();
                                double t = Benchmark.GetSeconds();
                                Console.WriteLine("TIME: " + t);
                                Console.WriteLine("TEXT: " + textOCR);

                                Benchmark.Start();
                                a = r.Next();
                                Bitmap bmp2 = (Bitmap)bitmap.Clone();
                                ResizeBilinear filterS2 = new ResizeBilinear(bmp2.Width * 4, bmp2.Height * 4);
                                // apply the filter
                                bmp2 = filterS2.Apply(bmp2);
                                Grayscale filterGS2 = new Grayscale(0, 1, 0);
                                // apply the filter
                                bmp2 = filterGS2.Apply(bmp2);
                                Threshold filter2 = new Threshold(160);
                                // apply the filter
                                filter2.ApplyInPlace(bmp2);
                                bmp2.Save("C:\\tmp\\imagen2_threshold" + a + ".bmp");
                                String textOCR2 = OCR(bmp2);
                                Benchmark.End();
                                double t2 = Benchmark.GetSeconds();
                                Console.WriteLine("TIME: " + t2);
                                Console.WriteLine("TEXT: " + textOCR2);

                                Benchmark.Start();
                                a = r.Next();
                                Bitmap bmp3 = (Bitmap)bitmap.Clone();
                                ResizeBilinear filterS3 = new ResizeBilinear(bmp3.Width * 4, bmp3.Height * 4);
                                // apply the filter
                                bmp3 = filterS3.Apply(bmp3);
                                Grayscale filterGS3 = new Grayscale(1, 0, 0);
                                // apply the filter
                                bmp3 = filterGS3.Apply(bmp3);
                                Threshold filter3 = new Threshold(160);
                                // apply the filter
                                filter3.ApplyInPlace(bmp3);
                                bmp3.Save("C:\\tmp\\imagen3_threshold" + a + ".bmp");
                                String textOCR3 = OCR(bmp3);
                                Benchmark.End();
                                double t3 = Benchmark.GetSeconds();
                                Console.WriteLine("TIME: " + t3);
                                Console.WriteLine("TEXT: " + textOCR3);

                                iteration++;
                                /*
                                if (iteration > 1)
                                {
                                    List<String> texts2 = getStringMap();
                                    foreach (String text2 in texts2)
                                    {
                                        Console.WriteLine(text2);
                                        if (text2 == "w")
                                        {
                                            Keyboard.KeyPress(Keys.W);
                                        }

                                        if (text2 == "a")
                                        {
                                            Keyboard.KeyPress(Keys.A);
                                        }

                                        if (text2 == "s")
                                        {
                                            Keyboard.KeyPress(Keys.S);
                                        }

                                        if (text2 == "d")
                                        {
                                            Keyboard.KeyPress(Keys.D);
                                        }
                                        Thread.Sleep(300);

                                    }
                                    if (texts2.Count > 0)
                                    {
                                        bitmap.Save("C:\\tmp\\arrows_wb_acierto" + a + ".bmp");
                                        Thread.Sleep(1000);
                                        Keyboard.KeyPress(Keys.R);
                                        Thread.Sleep(1000);
                                        encontrado = true;
                                        aciertos++;
                                        return;
                                    }
                                }*/
                                if (iteration > 7)
                                {
                                    bitmap.Save("C:\\tmp\\arrows_wb_fallo" + a + ".bmp");
                                    encontrado = true;
                                    fallos++;
                                    iteration = 0;
                                    Thread.Sleep(1000);
                                    Keyboard.KeyPress(Keys.R);
                                    Thread.Sleep(1000);
                                    return;
                                }

                            }
                            break;
                        default:
                            if (screenshot != null && screenshot.Data != null)
                            {
                                bitmap = screenshot.ToBitmap();
                                return;
                            }
                            break;
                    }
                    Thread thread = new Thread(() => DoRequest());
                    thread.Start();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
        }

        public static int colorEverage(Bitmap bitmap)
        {
            BitmapData srcData = bitmap.LockBits(
            new Rectangle(0, 0, bitmap.Width, bitmap.Height),
            ImageLockMode.ReadOnly,
            PixelFormat.Format24bppRgb);

            int stride = srcData.Stride;

            IntPtr Scan0 = srcData.Scan0;

            long[] totals = new long[] { 0 };

            int width = bitmap.Width;
            int height = bitmap.Height;

            unsafe
            {
                byte* p = (byte*)(void*)Scan0;
                Parallel.For(0, height, y =>
                {
                    Parallel.For(0, width, x =>
                    {
                        for (int color = 0; color < 1; color++)
                        {
                            int idx = (y * stride) + x * 3 + color;

                            totals[color] += p[idx];
                        }
                    });
                });
            }

            return (int)(totals[0] / (width * height));
        }

        public static Bitmap MakeGrayscale3(Bitmap original)
        {
            //create a blank bitmap the same size as original
            Bitmap newBitmap = new Bitmap(original.Width, original.Height);

            //get a graphics object from the new image
            Graphics g = Graphics.FromImage(newBitmap);

            //create the grayscale ColorMatrix
            ColorMatrix colorMatrix = new ColorMatrix(
               new float[][]
               {
         new float[] {.3f, .3f, .3f, 0, 0},
         new float[] {.59f, .59f, .59f, 0, 0},
         new float[] {.11f, .11f, .11f, 0, 0},
         new float[] {0, 0, 0, 1, 0},
         new float[] {0, 0, 0, 0, 1}
               });

            //create some image attributes
            ImageAttributes attributes = new ImageAttributes();

            //set the color matrix attribute
            attributes.SetColorMatrix(colorMatrix);

            //draw the original image on the new image
            //using the grayscale color matrix
            g.DrawImage(original, new Rectangle(0, 0, original.Width, original.Height),
               0, 0, original.Width, original.Height, GraphicsUnit.Pixel, attributes);

            //dispose the Graphics object
            g.Dispose();
            return newBitmap;
        }
        private static int codigo = 99999999;
        private static String text;
        private static bool encontrado = false;

        public static void OnApplicationExit(object sender, EventArgs e)
        {
            if (_captureProcess != null)
            {
                HookManager.RemoveHookedProcess(_captureProcess.Process.Id);
                _captureProcess.CaptureInterface.Disconnect();
                _captureProcess = null;
                Console.WriteLine("UNHOOK COMPLETO");
                Environment.Exit(1);
            }
        }


        public static void Main(string[] args)
        {
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(OnApplicationExit);
            form = new Form1();
            Thread thread = new Thread(() => logInit(form));
            thread.Start();
            Thread.Sleep(1000);
            AttachProcess();
            CapturarPrograma();
            Thread.Sleep(1000);
            if (bitmap != null)
            {
                while (true)
                {
                    Thread.Sleep(1000);
                    //Abrimos inventario
                    form.LogTextEvent("Abriendo Inventario");
                    Keyboard.KeyPress(Keys.I);
                    Thread.Sleep(1000);
                    codigo = 0;
                    rect = new Rectangle(1450, 200, 450, 700);
                    CapturarPrograma();
                    Thread.Sleep(1000);
                    //Seleccionamos y equipamos una caña
                    System.Drawing.Point point = new System.Drawing.Point(0, 0);
                    for (int i = 1; i < 6; i++)
                    {
                        Image<Bgr, byte> source = new Image<Bgr, byte>(bitmap); // Image B
                        Image<Bgr, byte> template = new Image<Bgr, byte>("Resources/Rods/poll_" + i + ".PNG"); // Image A
                        point = Compare(source, template, 0.90);
                        if (point != new System.Drawing.Point(0, 0))
                        {
                            continue;
                        }
                    }
                    point = new System.Drawing.Point(point.X + rect.X - 5, point.Y + rect.Y + 5);
                    form.LogTextEvent("Buscando Caña");
                    Thread.Sleep(1000);
                    form.LogTextEvent("Caña en :" + point);
                    Thread.Sleep(1000);
                    //form.LogTextEvent("Seleccionando Caña");
                    SetAndGoRight(point);
                    Thread.Sleep(1000);
                    //Cerramos el inventario
                    form.LogTextEvent("Cerrando Inventario");
                    Keyboard.KeyPress(Keys.I);
                    Thread.Sleep(500);
                    //Iniciamos la pesca
                    form.LogTextEvent("Iniciando Pesca");
                    Keyboard.KeyPress(Keys.Space);
                    point = new System.Drawing.Point(0, 0);
                    Thread.Sleep(500);
                    codigo = 1;
                    text = "";
                    rect = new Rectangle(625, 65, 600, 50);
                    CapturarPrograma();
                    while (!encontrado) { }
                    encontrado = false;
                    //Iniciamos el minijuego
                    form.LogTextEvent("He cogido algo!!!");
                    point = new System.Drawing.Point(0, 0);
                    Keyboard.KeyPress(Keys.Space);
                    Thread.Sleep(1670);
                    Keyboard.KeyPress(Keys.Space);
                    //Minijuego flechas
                    Thread.Sleep(3000);
                    /*
                    codigo = -1;
                    CapturarPrograma();
                    Image<Bgr, byte> source2 = new Image<Bgr, byte>(bitmap); // Image B
                    Image<Bgr, byte> template2 = new Image<Bgr, byte>("top_left_bar.bmp"); // Image A
                    point = Compare(source2, template2, 0.5);
                    form.LogTextEvent("Esquina en :" + point);*/
                    rect = new Rectangle(775, 345, 400, 100);
                    codigo = 3;
                    CapturarPrograma();
                    text = "";
                    Thread.Sleep(1000);
                    while (!encontrado) { }
                    encontrado = false;
                    Console.WriteLine("ACIERTOS: " + aciertos + " FALLOS: " + fallos);
                }
            }


        }
    }
}